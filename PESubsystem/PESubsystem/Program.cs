﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PESubsystem
{
    class Program
    {

        static short ReadInt16(Stream str)
        {
            byte[] IntBytes = new byte[2];
            str.Read(IntBytes, 0x00, 0x2);
            return BitConverter.ToInt16(IntBytes, 0x0);
        }

        static void WriteInt16(Stream str, short shr)
        {
            byte[] IntBytes = BitConverter.GetBytes(shr);
            str.Write(IntBytes, 0x00, 0x2);
        }
        static int ReadInt32(Stream str)
        {
            byte[] IntBytes = new byte[4];
            str.Read(IntBytes, 0x00, 0x4);
            return BitConverter.ToInt32(IntBytes,0x0);
        }
        static void Main(string[] args)
        {
            string FileName = "";
            if(args.Length >= 1)
            {
                FileName = args[0];
            }
            else
            {
                Console.Write("Enter Filepath: ");
                FileName = Console.ReadLine();
            }

            FileStream fs = new FileStream(FileName, FileMode.Open, FileAccess.ReadWrite);

            fs.Seek(0x3C, SeekOrigin.Begin);
            int PELocation = ReadInt32(fs);

            Console.WriteLine("PE Header Location: " + PELocation.ToString());

            fs.Seek(PELocation, SeekOrigin.Begin);
            fs.Seek(0x5C, SeekOrigin.Current);

            int Subsystem = ReadInt16(fs);

            string Subsystem_Str = "";

            switch(Subsystem)
            {
                case 1:
                    Subsystem_Str = "Win32_DRIVER";
                    break;
                case 2:
                    Subsystem_Str = "Win32_GUI";
                    break;
                case 3:
                    Subsystem_Str = "Win32_CUI";
                    break;
                default:
                    Subsystem_Str = "UNKNOWN";
                    break;
            }

            Console.WriteLine("PE Subsystem: " + Subsystem.ToString() + " ("+ Subsystem_Str+")");

            short NewSubsys = 0;

            if (args.Length >= 2)
            {
                NewSubsys = Int16.Parse(args[1]);
            }
            else
            {
                Console.WriteLine("\nSubsystems:\n1) WIN32_DRIVER\n2) WIN32_GUI\n3) WIN32_CUI\n\nEnter new subsystem value: ");
                NewSubsys = Int16.Parse(Console.ReadLine());
            }

            fs.Seek(-2, SeekOrigin.Current);
            WriteInt16(fs, NewSubsys);

            Console.WriteLine("Done! Subsystem value changed to: " + NewSubsys + "\nBlessed Be!");
        }
    }
}
